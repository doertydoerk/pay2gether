//
//  ConfigTest.swift
//  pay2getherTests
//
//  Created by Dirk Gerretz on 14.01.18.
//  Copyright © 2018 [code2 app];. All rights reserved.
//

@testable import pay2gether
import XCTest

class ConfigTest: XCTestCase {
    override func setUp() {
        UserDefaults.standard.removeObject(forKey: "name1")
        UserDefaults.standard.removeObject(forKey: "name2")
        UserDefaults.standard.removeObject(forKey: "threshold")
    }

    override func tearDown() {
        UserDefaults.standard.removeObject(forKey: "name1")
        UserDefaults.standard.removeObject(forKey: "name2")
        UserDefaults.standard.removeObject(forKey: "threshold")
    }

    func testWriteAndReadDefaults() {
        // write settings
        Config.name1 = "Han"
        Config.name2 = "Lea"
        Config.threshold = 10
        XCTAssertTrue(Config.writeUserDefaults())

        // reset but don't write
        Config.name1 = ""
        Config.name2 = ""
        Config.threshold = 0

        // read back
        Config.readUserDefaults()
        XCTAssertTrue(Config.name1 == "Han", "")
        XCTAssertTrue(Config.name2 == "Lea", "")
        XCTAssertTrue(Config.threshold == 10, "")
    }

    func testReadEmptyUserDefaults() {
        // this test makes sures that no crash occurse when defaults are nil
        Config.name1 = ""
        Config.name2 = ""
        Config.threshold = 0

        Config.readUserDefaults()

        XCTAssertTrue(Config.name1 == "", "")
        XCTAssertTrue(Config.name2 == "", "")
        XCTAssertTrue(Config.threshold == 0, "")
    }
}
