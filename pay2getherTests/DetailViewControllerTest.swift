//
//  DetailViewControllerTest.swift
//  pay2getherTests
//
//  Created by Dirk Gerretz on 24.01.18.
//  Copyright © 2018 [code2 app];. All rights reserved.
//

@testable import pay2gether
import XCTest

class DetailViewControllerTest: XCTestCase {
    var sut: DetailViewController?

    override func setUp() {
        super.setUp()
        let navBar = NavigationControllerMock()
        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
        sut = storyBoard.instantiateViewController(withIdentifier: "DetailView") as? DetailViewController
        sut?.dto = TransactionDTO()
        XCTAssertNotNil(sut, "View controller is nil")

        navBar.pushViewController(sut!, animated: false)
        sut?.loadView()
        sut?.viewDidLoad()

        XCTAssertNotNil(sut?.presenter, "Presenter should not be nil")
        XCTAssertTrue(sut!.datePicker.isHidden)
        XCTAssertNotNil(sut?.view)

        // check private method setup(_ button:) was called
        XCTAssertTrue(sut!.doneButton.clipsToBounds)
    }

    func testViewWillAppear() {
        let navBar = sut?.navigationController as? NavigationControllerMock
        sut?.viewWillAppear(false)
        XCTAssertTrue(navBar!.setNavigationBarHiddenWasCalled)
    }

    func testViewDidDisappear() {
        let navBar = sut?.navigationController as? NavigationControllerMock
        sut?.viewDidDisappear(false)
        XCTAssertTrue(navBar!.setNavigationBarHiddenWasCalled)
    }

    func testSaveDto() {
        // save invalid DTO
        let presenter = DetailPresenterMock(view: nil)
        sut?.presenter = presenter
        sut?.dto = TransactionDTO()
        sut?.saveItem()
        XCTAssertFalse(presenter.saveItemWasCalled)

        // save valid DTO
        sut?.dateTextfield.text = "\(NSDate())"
        sut?.spenderTextfield.text = "John"
        sut?.purposeTextfield.text = "Unit Test"
        sut?.amountTextfield.text = "10"
        sut?.saveItem()
        XCTAssertTrue(presenter.saveItemWasCalled)
    }

    func testPopulateTextfields() {
        sut?.dto = TransactionDTO(created: NSDate(),
                                  spender: "name1",
                                  amount: Int32(0),
                                  date: NSDate(),
                                  purpose: "Test")

        sut?.viewDidLoad()
        sut?.viewWillAppear(false)
        XCTAssertTrue((sut?.purposeTextfield.text = "Test") != nil)
        XCTAssertTrue((sut?.spenderTextfield.text = "name1") != nil)

        sut?.dto = TransactionDTO(created: NSDate(),
                                  spender: "name2",
                                  amount: 5,
                                  date: NSDate(),
                                  purpose: "Test")

        sut?.viewDidLoad()
        sut?.viewWillAppear(false)
        XCTAssertTrue((sut?.purposeTextfield.text = "Test") != nil)
        XCTAssertTrue((sut?.spenderTextfield.text = "name2") != nil)
    }

    func testIsValidTransaction() {
        sut?.dateTextfield.text = ""
        sut?.spenderTextfield.text = "John"
        sut?.purposeTextfield.text = "Unit Test"
        sut?.amountTextfield.text = "10"
        XCTAssertFalse((sut?.isValidTransaction())!)

        sut?.dateTextfield.text = "\(NSDate())"
        sut?.spenderTextfield.text = ""
        sut?.purposeTextfield.text = "Unit Test"
        sut?.amountTextfield.text = "10"
        XCTAssertFalse((sut?.isValidTransaction())!)

        sut?.dateTextfield.text = "\(NSDate())"
        sut?.spenderTextfield.text = "John"
        sut?.purposeTextfield.text = ""
        sut?.amountTextfield.text = "10"
        XCTAssertFalse((sut?.isValidTransaction())!)

        sut?.dateTextfield.text = "\(NSDate())"
        sut?.spenderTextfield.text = "John"
        sut?.purposeTextfield.text = "Unit Test"
        sut?.amountTextfield.text = ""
        XCTAssertFalse((sut?.isValidTransaction())!)

        sut?.dateTextfield.text = "\(NSDate())"
        sut?.spenderTextfield.text = "John"
        sut?.purposeTextfield.text = "Unit Test"
        sut?.amountTextfield.text = "0"
        XCTAssertFalse((sut?.isValidTransaction())!)

        sut?.dateTextfield.text = "\(NSDate())"
        sut?.spenderTextfield.text = "John"
        sut?.purposeTextfield.text = "Unit Test"
        sut?.amountTextfield.text = "10"
        XCTAssertTrue((sut?.isValidTransaction())!)
    }

    func testDoneButtonTapped() {
        let sut = DetailViewControllerMock()
        sut.presenter = DetailPresenterMock(view: nil)
        sut.dto = TransactionDTO()
        sut.loadView()
        sut.viewDidLoad()
        sut.doneTapped(UIButton())
        XCTAssertTrue(sut.isValidTransactionWasCalled)
    }

    func testBackButtonTapped() {
        let navController = NavigationControllerMock()
        navController.pushViewController(sut!, animated: false)
        sut?.backTapped(UIButton())
        XCTAssertTrue(navController.popViewControllerWasCalled)
    }

    func testDatePickerChanged() {
        let date = Date()
        sut?.datePicker.date = date
        sut?.datePickerChanged(sender: (sut?.datePicker)!)
        XCTAssertTrue(sut?.dateTextfield.text == Date.toString(date))
        XCTAssertTrue(sut?.dto?.date == (date as NSDate))
    }

    func testTextfieldShouldReturn() {
        let sut = DetailViewControllerMock()
        sut.presenter = DetailPresenterMock(view: sut)
        sut.dto = TransactionDTO()
        _ = sut.textFieldShouldReturn(UITextField())
        XCTAssertTrue(sut.isValidTransactionWasCalled)
    }

    func testTextfieldShouldBeginEditing_spender() {
        sut?.dto?.spender = ""
        Config.name1 = "John"
        Config.name1 = "Jane"

        sut?.spenderTextfield.text = "John"
        XCTAssertFalse(sut!.textFieldShouldBeginEditing(sut!.spenderTextfield))

        sut?.dto?.spender = ""
        sut?.spenderTextfield.text = "Jane"
        XCTAssertFalse(sut!.textFieldShouldBeginEditing(sut!.spenderTextfield))
    }

    func testTextfieldShouldBeginEditing_Amount() {
        sut?.datePicker.isHidden = true
        _ = sut?.textFieldShouldBeginEditing(sut!.dateTextfield)
        XCTAssertFalse((sut?.datePicker.isHidden)!)
    }

    func testTextfieldShouldBeginEditing_Guard() {
        XCTAssertTrue((sut?.textFieldShouldBeginEditing(sut!.amountTextfield))!)
    }

    func testTextfieldDidEndEditing() {
        sut?.purposeTextfield.text = "Test"
        sut?.textFieldDidEndEditing((sut?.purposeTextfield)!)
        XCTAssertTrue(sut?.dto?.purpose == "Test")

        sut?.amountTextfield.text = "10"
        sut?.textFieldDidEndEditing((sut?.amountTextfield)!)
        XCTAssertTrue(sut?.dto?.amount == 10)
    }
}

private class DetailPresenterMock: DetailPresenter {
    var saveItemWasCalled = false

    override func newTransactionFrom(_: TransactionDTO) {
        saveItemWasCalled = true
    }
}

private class DetailViewControllerMock: DetailViewController {
    var isValidTransactionWasCalled = false
    var viewDidLoadWasCalled = false

    override func viewDidLoad() {
        // NOTE: do not call super.viewDidLoad() !!
        viewDidLoadWasCalled = true
    }

    override func isValidTransaction() -> Bool {
        isValidTransactionWasCalled = true
        return true
    }
}

private class NavigationControllerMock: UINavigationController {
    var setNavigationBarHiddenWasCalled = false
    var popViewControllerWasCalled = false

    override func setNavigationBarHidden(_: Bool, animated _: Bool) {
        setNavigationBarHiddenWasCalled = true
    }

    override func popViewController(animated _: Bool) -> UIViewController? {
        popViewControllerWasCalled = true
        return UIViewController()
    }
}
