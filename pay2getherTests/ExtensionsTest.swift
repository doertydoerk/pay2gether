//
//  ExtensionsTest.swift
//  pay2getherTests
//
//  Created by Dirk Gerretz on 16.12.17.
//  Copyright © 2017 [code2 app];. All rights reserved.
//

@testable import pay2gether
import XCTest

class ExtensionsTest: XCTestCase {
    func testRounding() {
        var pi = 3.1415926535
        XCTAssertTrue(pi.roundOneDecimal() == 3.1, "Rounding failed")
    }

    // TODO: test all other extensions
}
