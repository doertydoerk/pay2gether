//
//  BaseTest.swift
//  pay2getherTests
//
//  Created by Dirk Gerretz on 13.11.17.
//  Copyright © 2017 [code2 app];. All rights reserved.
//

import CoreData
@testable import pay2gether
import XCTest

class BaseTest: XCTestCase {
    // MARK: - Properties

    let dataManager = CoreDataManager.shared
    var helper: TestHelper!

    // MARK: - Prep

    override func setUp() {
        super.setUp()

        // remove all prior items from context
        XCTAssertTrue(dataManager.deleteAllTransactions())
        // save empty context
        dataManager.persistData()

        // get Test Helper
        helper = TestHelper(dataManager: dataManager)
    }

    override func tearDown() {
        // remove all items from context
        _ = dataManager.deleteAllTransactions()
        // save empty context
        dataManager.persistData()
        super.tearDown()
    }
}
