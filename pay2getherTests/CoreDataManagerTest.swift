//
//  CoreDataManagerTest.swift
//  pay2getherTests
//
//  Created by Dirk Gerretz on 06.11.17.
//  Copyright © 2017 [code2 app];. All rights reserved.
//

import CoreData
@testable import pay2gether
import XCTest

class CoreDataManagerTest: BaseTest {
    // MARK: - Properties

    // MARK: - Prep

    override func setUp() {
        super.setUp()

        // remove all prior items from context
        _ = dataManager.deleteAllTransactions()

        // get Test Helper
        helper = TestHelper(dataManager: dataManager)
    }

    override func tearDown() {
        // remove all items from context
        _ = dataManager.deleteAllTransactions()
        // save empty context
        dataManager.persistData()
        super.tearDown()
    }

    // MARK: - Tests

    func testSingleton() {
        let second = CoreDataManager.shared
        XCTAssertTrue(dataManager === second, "CoreDataManager instances not identical")
    }

    func testNewTransactionItem() {
        XCTAssertTrue(dataManager.countTransactions() == 0)
        let transaction = helper.createTestTransaction()
        XCTAssertNotNil(transaction)
        XCTAssertTrue(dataManager.countTransactions() == 1)
    }

    func testDeleteSingleTransaction() {
        XCTAssertTrue(dataManager.countTransactions() == 0)
        let ta = helper.createTestTransaction()
        XCTAssertTrue(dataManager.countTransactions() == 1)
        dataManager.delete(transaction: ta)
        XCTAssertTrue(dataManager.countTransactions() == 0)
    }

    func testDeleteAllTransactionsSuccess() {
        helper.createTestTransactions(10)
        var transactions = dataManager.fetchAllTransactions()

        XCTAssertTrue(dataManager.countTransactions() == 10)
        XCTAssertTrue(transactions.count == 10)

        XCTAssertTrue(dataManager.deleteAllTransactions())
        transactions = dataManager.fetchAllTransactions()

        XCTAssertTrue(dataManager.countTransactions() == 0)
        XCTAssertTrue(transactions.count == 0)
    }

    func testDeleteAllTransactionsFailure() {
        let sutMock = DataManagerMock()
        let ta = sutMock.newTransaction()
        ta.spender = "name1"
        sutMock.persistData()
        XCTAssertFalse(sutMock.deleteAllTransactions())
    }

    func testFetchAll() {
        helper.createTestTransactions(10)
        var transactions = dataManager.fetchAllTransactions()
        transactions.removeAll()
        XCTAssertTrue(transactions.count == 0)

        transactions = dataManager.fetchAllTransactions()
        XCTAssertTrue(transactions.count == 10)
    }

    func testFindTransactionSuccess() {
        let ta1 = helper.createTestTransaction()
        let now = NSDate()
        ta1.created = now
        let ta2 = helper.createTestTransaction()

        let taResult = dataManager.findTransactionBy(creationDate: now)
        XCTAssertTrue(taResult == ta1, "Transactions do not match, but should")
        XCTAssertFalse(taResult == ta2, "Transactions do match, but should not")
    }

    func testFindTransactonFailure() {
        let now = NSDate()
        XCTAssertNil(dataManager.findTransactionBy(creationDate: now), "Should be nil")
    }
}

private class DataManagerMock: CoreDataManager {
    override func countTransactions() -> Int {
        1
    }
}
