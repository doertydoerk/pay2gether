//
//  SettingsPresenterTest.swift
//  pay2getherTests
//
//  Created by Dirk Gerretz on 20.01.18.
//  Copyright © 2018 [code2 app];. All rights reserved.
//

@testable import pay2gether
import XCTest

class SettingsPresenterTest: BaseTest {
    // MARK: - Properties

    let sut = SettingsPresenter(view: nil)

    override func setUp() {
        super.setUp()
        XCTAssertNotNil(sut, "SUT is nil")
        XCTAssertNil(sut.view, "Should be nil")
    }

    // MARK: - Tests

    func testResetHistory() {
        let expt = expectation(description: "Reset History")
        sut.dataManager = CoreDataManagerMock(expt: expt) as CoreDataManagerMock

        XCTAssertTrue(sut.resetHistory())
        waitForExpectations(timeout: 1, handler: { _ in
            XCTAssertTrue((self.sut.dataManager as! CoreDataManagerMock).deleteAllWasCalled)
        })
    }
}

class CoreDataManagerMock: CoreDataManager {
    var expt: XCTestExpectation?
    var deleteAllWasCalled = false

    convenience init(expt: XCTestExpectation) {
        self.init()
        self.expt = expt
    }

    override init() {
        super.init()
    }

    override func deleteAllTransactions() -> Bool {
        deleteAllWasCalled = true
        expt?.fulfill()
        return true
    }
}
