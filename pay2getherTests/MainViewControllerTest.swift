//
//  MainViewControllerTest.swift
//  pay2getherTests
//
//  Created by Dirk Gerretz on 27.01.18.
//  Copyright © 2018 [code2 app];. All rights reserved.
//

@testable import pay2gether
import UIKit.UIGestureRecognizerSubclass
import XCTest

class MainViewControllerTest: BaseTest {
    // MARK: - Properties

    var sut: MainViewController?

    // MARK: - Lifecycle

    override func setUp() {
        super.setUp()
        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
        sut = storyBoard.instantiateViewController(withIdentifier: "MainViewID") as? MainViewController
        sut?.loadView()
        sut?.viewDidLoad()
    }

    override func tearDown() {
        sut = nil
        super.tearDown()
    }

    // MARK: - Tests

    func testViewWillAppear() {
        sut = MainViewControllerMock()
        let navCon = UINavigationController()
        navCon.pushViewController(sut!, animated: false)
        sut?.viewWillAppear(false)
        XCTAssertTrue(navCon.navigationBar.isHidden)
    }

    func testViewWillDisappear() {
        sut = MainViewControllerMock()
        let navCon = UINavigationController()
        navCon.pushViewController(sut!, animated: false)
        sut?.viewWillDisappear(false)
        XCTAssertFalse(navCon.navigationBar.isHidden)
    }

    func testSettingsTapped() {
        let presenter = MainPresenterMock()
        sut?.presenter = presenter
        sut?.settingsTapped(UIButton())
        XCTAssertTrue(presenter.segueToSettingsWasCalled)
    }

    func testBubbleTapped() {
        let presenter = MainPresenterMock()
        sut?.presenter = presenter

        sut?.bubbleTapped((sut?.tapRecognizer1)!)
        XCTAssertTrue(presenter.segueToItemWasCalled)

        presenter.segueToItemWasCalled = false
        sut?.bubbleTapped((sut?.tapRecognizer2)!)
        XCTAssertTrue(presenter.segueToItemWasCalled)
    }

    func testLongPressedEarlyReturn() {
        let tableViewMock = TableViewMock()
        sut?.tableView = tableViewMock
        sut?.longPressed((sut?.longPressRecognizer1)!)
        XCTAssertFalse(tableViewMock.reloadDataWasCalled)
    }

    func testGestureStateIsBeganFalse() {
        XCTAssertFalse((sut?.gestureStateIsBegan(gesture: (sut?.longPressRecognizer1)!))!)
    }

    func testMakeBigBubble() {
        sut?.makeSmall(bubble: 1)
        XCTAssertFalse(sut?.bubble1Container.frame == sut?.bubble1BigRect)
        sut?.makeSmall(bubble: 2)
        XCTAssertFalse(sut?.bubble2Container.frame == sut?.bubble2BigRect)

        sut?.makeBig(bubble: 1)
        XCTAssertTrue(sut?.bubble1Container.frame == sut?.bubble1BigRect)

        sut?.makeBig(bubble: 2)
        XCTAssertTrue(sut?.bubble2Container.frame == sut?.bubble2BigRect)
    }

    func testResizeBubble() {
        var account = Account()

        account.spender1Balance = 2
        account.spender2Balance = 1
        sut?.resizeBubbles(account: account)
        XCTAssertTrue(sut?.bubble1Container.frame == sut?.bubble1BigRect)
        XCTAssertTrue(sut?.bubble2Container.frame == sut?.bubble2SmallRect)

        account.spender1Balance = 1
        account.spender2Balance = 2
        sut?.resizeBubbles(account: account)
        XCTAssertTrue(sut?.bubble1Container.frame == sut?.bubble1SmallRect)
        XCTAssertTrue(sut?.bubble2Container.frame == sut?.bubble2BigRect)
    }

    func testIssueThresholdAlert() {
        let sutMock = MainViewControllerMock()
        sutMock.presenter = MainPresenterMock()
        sutMock.issueThresholdAlert()
        XCTAssertTrue(sutMock.presentWasCalled)
    }

    func testRefreshAccount() {
        let tableView = TableViewMock()
        sut?.tableView = tableView

        var transactions: [Transaction] = []
        for i in 0 ..< 5 {
            let ta = helper.createTestTransaction()
            if i % 2 == 1 {
                ta.spender = "name1"
            } else {
                ta.spender = "name2"
            }
            transactions.append(ta)
        }

        let account = AccountHelper.getAccount(transactions: transactions)
        sut?.refresh(account)
        XCTAssertTrue((sut?.tableView as! TableViewMock).reloadDataWasCalled)
    }

    func testUpdateTableView() {
        let tableView = TableViewMock()
        sut?.tableView = tableView
        sut?.updateTableView()
        XCTAssertTrue((sut?.tableView as! TableViewMock).reloadDataWasCalled)
    }

    func testrequestUpddate() {
        let tableView = TableViewMock()
        sut?.tableView = tableView

        sut?.requestUpdate()
        XCTAssertTrue((sut?.tableView as! TableViewMock).reloadDataWasCalled)
    }

    func testTableViewTitleForHeader() {
        sut?.historyForSpender = ""
        var title = (sut?.tableView((sut?.tableView)!, titleForHeaderInSection: 0))
        XCTAssertTrue(title == "All Transactions ")

        sut?.historyForSpender = Config.name1
        title = (sut?.tableView((sut?.tableView)!, titleForHeaderInSection: 0))
        XCTAssertTrue(title == "\(Config.name1)'s Transactions ")

        sut?.historyForSpender = Config.name2
        title = (sut?.tableView((sut?.tableView)!, titleForHeaderInSection: 0))
        XCTAssertTrue(title == "\(Config.name2)'s Transactions ")
    }
}

class TableViewMock: UITableView {
    var reloadDataWasCalled = false

    override func reloadData() {
        reloadDataWasCalled = true
    }
}

class MainPresenterMock: MainPresenter {
    var segueToSettingsWasCalled = false
    var segueToItemWasCalled = false
    var deleteAllTransactionsWasCalled = false
    var requestUpdateWasCalled = false

    override func segueToSettingsView() {
        segueToSettingsWasCalled = true
    }

    override func segueToNewItemFor(spender _: UInt) {
        segueToItemWasCalled = true
    }

    override func deleteAllTransactions() {
        deleteAllTransactionsWasCalled = true
    }
}

class MainViewControllerMock: MainViewController {
    var presentWasCalled = false

    override func viewDidLoad() {
        // DO NOT CALL super.viewDidLoad()
        longPressRecognizer1 = UILongPressGestureRecognizer()
        longPressRecognizer2 = UILongPressGestureRecognizer()
        presenter = MainPresenterMock()
    }

    override func viewDidAppear(_: Bool) {
        // DO NOT CALL super.viewDidLoad()
    }

    override func present(_: UIViewController, animated _: Bool, completion _: (() -> Void)? = nil) {
        presentWasCalled = true
    }
}
