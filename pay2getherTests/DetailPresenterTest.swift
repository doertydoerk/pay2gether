//
//  DetailPresenterTest.swift
//  pay2getherTests
//
//  Created by Dirk Gerretz on 02.12.17.
//  Copyright © 2017 [code2 app];. All rights reserved.
//

@testable import pay2gether
import XCTest

class DetailPresenterTest: BaseTest {
    let sut = DetailPresenter(view: nil)

    override func setUp() {
        super.setUp()
        XCTAssertNotNil(sut, "Presenter is nil")
        XCTAssertNil(sut.view, "Presenter View should be nil")
        XCTAssertNotNil(sut.dataManager, "Presenter DataManager is nil")
    }

    func testSaveNewDtoName1() {
        var dto = TransactionDTO()
        dto.date = NSDate()
        dto.spender = "name1"
        dto.amount = 10
        dto.purpose = "Test"
        sut.save(dto)

        let transactions = sut.dataManager.fetchAllTransactions()
        XCTAssertTrue(transactions.count == 1, "Count should be 1")
        XCTAssertTrue(transactions[0].amount == 10, "Amount should be 10")
        XCTAssertTrue(transactions[0].spender == "name1", "Name should be 'name1'")
        XCTAssertTrue(transactions[0].purpose == "Test", "Purpose should be 'Test'")
    }

    func testSaveNewDtoName2() {
        var dto = TransactionDTO()
        dto.date = NSDate()
        dto.spender = "name2"
        dto.amount = 12
        dto.purpose = "Test2"
        sut.save(dto)

        let transactions = sut.dataManager.fetchAllTransactions()
        XCTAssertTrue(transactions.count == 1, "Count should be 1")
        XCTAssertTrue(transactions[0].amount == 12, "Amount should be 12")
        XCTAssertTrue(transactions[0].spender == "name2", "Name should be 'name2'")
        XCTAssertTrue(transactions[0].purpose == "Test2", "Purpose should be 'Test2'")
    }

    func testUpdateTransactionWithDtoName1() {
        let created = NSDate()

        // create original transaction
        let ta = sut.dataManager.newTransaction()
        ta.created = created
        ta.date = NSDate()
        ta.spender = "name1"
        ta.amount = 10
        ta.purpose = "Test"

        // create the DTO to alter the transaction with
        var dto = TransactionDTO()
        dto.created = created
        dto.date = NSDate()
        dto.spender = "name2"
        dto.amount = 12
        dto.purpose = "New Test"
        sut.save(dto)

        let alteredTa = sut.dataManager.findTransactionBy(creationDate: created)
        if let _alteredTa = alteredTa {
            XCTAssertTrue(_alteredTa.amount == 12)
            XCTAssertTrue(_alteredTa.purpose == dto.purpose)
            XCTAssertTrue(_alteredTa.spender == "name2")
        } else {
            XCTFail("Transaction is nil")
        }
    }

    func testUpdateTransactionWithDtoName2() {
        let created = NSDate()

        // create original transaction
        let ta = sut.dataManager.newTransaction()
        ta.created = created
        ta.date = NSDate()
        ta.spender = "name2"
        ta.amount = 10
        ta.purpose = "Test"

        // create the DTO to alter the transaction with
        var dto = TransactionDTO()
        dto.created = created
        dto.date = NSDate()
        dto.spender = "name1"
        dto.amount = 12
        dto.purpose = "New Test"
        sut.save(dto)

        let alteredTa = sut.dataManager.findTransactionBy(creationDate: created)
        if let _alteredTa = alteredTa {
            XCTAssertTrue(_alteredTa.amount == 12)
            XCTAssertTrue(_alteredTa.purpose == dto.purpose)
            XCTAssertTrue(_alteredTa.spender == "name1")
        } else {
            XCTFail("Transaction is nil")
        }
    }
}
