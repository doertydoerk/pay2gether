//
//  NSDateExtenstionTest.swift
//  pay2getherTests
//
//  Created by Dirk Gerretz on 04.03.18.
//  Copyright © 2018 [code2 app];. All rights reserved.
//

@testable import pay2gether
import XCTest

class NSDateExtenstionTest: XCTestCase {
    let epochTime = UInt64(1_514_764_800_000)
    let expected = "2018-01-01 00:00:00 +0000"

    func testCurrentTimeMicroseconds() {
        let date = NSDate.dateFromMicroseconds(sec: epochTime)
        let result = NSDate.microsecondsFrom(date: date)
        XCTAssertTrue(result == (epochTime * 1000), "Incorrect milliseconds returned")
    }

    func testDateFromMicroseconds() {
        let date = NSDate.dateFromMicroseconds(sec: epochTime)
        print("\(date)")
        XCTAssertTrue(expected == "\(date)", "Returned date is unexpected.")
    }
}
