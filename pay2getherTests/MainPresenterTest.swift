//
//  MainPresenterTest.swift
//  pay2getherTests
//
//  Created by Dirk Gerretz on 02.12.17.
//  Copyright © 2017 [code2 app];. All rights reserved.
//

@testable import pay2gether
import XCTest

class MainPresenterTest: BaseTest {
    // MARK: - Properties

    let sut = MainPresenter()

    // MARK: - Liefecycle

    override func setUp() {
        super.setUp()
        XCTAssertNotNil(sut)
        XCTAssertNil(sut.view)
    }

    // MARK: - Tests

    func testSegueToNewItem() {
        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
        let view = storyBoard.instantiateViewController(withIdentifier: "MainViewID") as? MainViewController
        view?.loadView()
        view?.viewDidLoad()
        sut.view = view

        let navigationController = NavControllerMock()
        navigationController.viewControllers = [view!]

        sut.segueToNewItemFor(spender: 1)
        XCTAssertTrue(navigationController.pushWasCalled)

        navigationController.pushWasCalled = false
        sut.segueToNewItemFor(spender: 2)
        XCTAssertTrue(navigationController.pushWasCalled)
    }

    func testSegueToEditItem() {
        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
        let view = storyBoard.instantiateViewController(withIdentifier: "MainViewID") as? MainViewController
        view?.loadView()
        view?.viewDidLoad()
        sut.view = view

        let navigationController = NavControllerMock()
        navigationController.viewControllers = [view!]

        let dto = TransactionDTO()
        sut.segueToEditItem(dto)
        XCTAssertTrue(navigationController.pushWasCalled)
    }

    func testSegueToSetting() {
        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
        let view = storyBoard.instantiateViewController(withIdentifier: "MainViewID") as? MainViewController
        view?.loadView()
        view?.viewDidLoad()
        sut.view = view

        let navigationController = NavControllerMock()
        navigationController.viewControllers = [view!]

        sut.segueToSettingsView()
        XCTAssertTrue(navigationController.pushWasCalled)
    }

    func testDeleteTransactionWithDate() {
        let ta = sut.dataManager.newTransaction()
        let date = ta.created
        XCTAssertTrue(sut.dataManager.countTransactions() == 1)
        sut.deleteTransactionWith(creationDate: date!)
        XCTAssertTrue(sut.dataManager.countTransactions() == 0)
    }

    func testDeleteAllTransactions() {
        helper.createTestTransactions(2)
        XCTAssertTrue(sut.dataManager.countTransactions() == 2)
        sut.deleteAllTransactions()
        XCTAssertTrue(sut.dataManager.countTransactions() == 0)
    }

    func testDtoForTransactionName1() {
        let ta = dataManager.newTransaction()
        let date = NSDate()
        ta.created = date
        ta.date = date
        ta.amount = 10
        ta.spender = "name1"
        ta.purpose = "Test"

        let dto = sut.dtoForTransaction(withCreationDate: ta.created!)
        XCTAssertTrue(dto?.amount == 10)
        XCTAssertTrue(dto?.spender == "name1")
        XCTAssertTrue(dto?.purpose == "Test")
        XCTAssertTrue(dto?.created == date)
        XCTAssertTrue(dto?.date == date)

        XCTAssertNil(sut.dtoForTransaction(withCreationDate: NSDate()))
    }

    func testDtoForTransactionName2() {
        let ta = dataManager.newTransaction()
        let date = NSDate()
        ta.created = date
        ta.date = date
        ta.amount = 12
        ta.spender = "name2"
        ta.purpose = "Test2"

        let dto = sut.dtoForTransaction(withCreationDate: ta.created!)
        XCTAssertTrue(dto?.amount == 12)
        XCTAssertTrue(dto?.spender == "name2")
        XCTAssertTrue(dto?.purpose == "Test2")
        XCTAssertTrue(dto?.created == date)
        XCTAssertTrue(dto?.date == date)
    }

    func testTableViewDidSelectRow() {
        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
        let view = storyBoard.instantiateViewController(withIdentifier: "MainViewID") as? MainViewController
        view?.loadView()
        view?.viewDidLoad()
        sut.view = view

        let navigationController = NavControllerMock()
        navigationController.viewControllers = [view!]

        _ = sut.dataManager.newTransaction()
        sut.view?.updateTableView()

        let indexPath = IndexPath(row: 0, section: 0)
        XCTAssertNotNil(sut.view?.tableView.cellForRow(at: indexPath))
        sut.tableView((sut.view?.tableView)!, didSelectRowAt: indexPath)
        XCTAssertTrue(navigationController.pushWasCalled)
    }
}

private class NavControllerMock: UINavigationController {
    var pushWasCalled = false

    override func pushViewController(_: UIViewController, animated _: Bool) {
        pushWasCalled = true
    }
}

private class DetailViewControllerMock: DetailViewController {
    var viewDidLoadWasCalled = false

    override func viewDidLoad() {
        viewDidLoadWasCalled = true
    }
}
