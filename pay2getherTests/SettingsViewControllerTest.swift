//
//  SettingsViewControllerTest.swift
//  pay2getherTests
//
//  Created by Dirk Gerretz on 18.02.18.
//  Copyright © 2018 [code2 app];. All rights reserved.
//

@testable import pay2gether
import XCTest

class SettingsViewControllerTest: BaseTest {
    var sut: SettingsViewController?

    override func setUp() {
        super.setUp()
        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
        sut = storyBoard.instantiateViewController(withIdentifier: "SettingsViewID") as? SettingsViewController
        sut?.loadView()
        sut?.viewDidLoad()
        XCTAssertTrue(sut?.name1Textfield.text == Config.name1)
        XCTAssertTrue(sut?.name2Textfield.text == Config.name2)
        //		  XCTAssertTrue(sut?.thresholdTextfield.text == "2")
    }

    override func tearDown() {
        sut?.thresholdTextfield.text = "2"
        sut?.saveSettings()
        super.tearDown()
    }

    func testViewWillDisappear() {
        sut?.name1Textfield.text = "ABC"
        sut?.viewWillDisappear(false)
        XCTAssertTrue(Config.name1 == "ABC")
    }

    func testSaveAndLoadSettings() {
        sut?.name1Textfield.text = "ABC"
        sut?.name2Textfield.text = "XYZ"
        sut?.thresholdTextfield.text = "2"
        sut?.saveSettings()
        XCTAssertTrue(Config.name1 == "ABC")
        XCTAssertTrue(Config.name2 == "XYZ")
        XCTAssertTrue(Config.threshold == 2)

        sut?.name1Textfield.text = ""
        sut?.name2Textfield.text = ""
        sut?.thresholdTextfield.text = "0"
        sut?.loadSettings()
        XCTAssertTrue(sut?.name1Textfield.text == "ABC")
        XCTAssertTrue(sut?.name2Textfield.text == "XYZ")
        XCTAssertTrue(sut?.thresholdTextfield.text == "2")
    }

    func testResetHistoryTapped() {
        let sut = SettingsViewControllerMock()
        sut.presenter = SettingsPresenter()
        sut.resetHistoryTapped(UIButton())
        XCTAssertTrue((sut as SettingsViewControllerMock).presentWasCalled)
    }
}

private class SettingsViewControllerMock: SettingsViewController {
    var presentWasCalled = false

    override func viewDidLoad() {
        // do nothing here for now
    }

    override func present(_: UIViewController, animated _: Bool, completion _: (() -> Void)? = nil) {
        presentWasCalled = true
    }
}
