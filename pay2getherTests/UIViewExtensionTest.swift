//
//  UIViewExtensionTest.swift
//  pay2getherTests
//
//  Created by Dirk Gerretz on 04.03.18.
//  Copyright © 2018 [code2 app];. All rights reserved.
//

@testable import pay2gether
import XCTest

class UIViewExtensionTest: XCTestCase {
    func testFadeOut() {
        let view = UIView()
        XCTAssertTrue(view.alpha == 1)
        view.fadeOut()
        XCTAssertTrue(view.alpha == 0)
    }
}
