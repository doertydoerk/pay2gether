//
//  AccountTest.swift
//  pay2getherTests
//
//  Created by Dirk Gerretz on 06.12.17.
//  Copyright © 2017 [code2 app];. All rights reserved.
//

@testable import pay2gether
import XCTest

class AccountTest: XCTestCase {
    // MARK: - Properties

    var account = Account()

    // MARK: - Tests

    func testDiffForSpender() {
        account.spender1Balance = 100
        account.spender2Balance = 50

        XCTAssertTrue(account.diffFor(spender: "name1") == "+ 50 €")
        XCTAssertTrue(account.diffFor(spender: "name2") == "")

        account.spender1Balance = 50
        account.spender2Balance = 100

        XCTAssertTrue(account.diffFor(spender: "name2") == "+ 50 €")
        XCTAssertTrue(account.diffFor(spender: "name1") == "")
    }

    func testDiffForSpenderIsLower() {
        account.spender1Balance = 50
        account.spender2Balance = 100
        XCTAssertTrue(account.diffFor(spender: "name1") == "")

        account.spender1Balance = 100
        account.spender2Balance = 50
        XCTAssertTrue(account.diffFor(spender: "name2") == "")
    }

    func testEqual() {
        account.spender1Balance = 100
        account.spender2Balance = 100
        XCTAssertTrue(account.diffFor(spender: "name1") == "")
        XCTAssertTrue(account.diffFor(spender: "name2") == "")
    }

    func testInvalidArgument() {
        XCTAssertTrue(account.diffFor(spender: "") == "")
    }
}
