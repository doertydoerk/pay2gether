//
//  DotTest.swift
//  pay2getherTests
//
//  Created by Dirk Gerretz on 27.01.18.
//  Copyright © 2018 [code2 app];. All rights reserved.
//

@testable import pay2gether
import XCTest

class DotTest: XCTestCase {
    func testDot() {
        let dot = Dot(frame: CGRect(x: 0, y: 0, width: 20, height: 20))
        dot.draw(dot.frame)
        XCTAssertNotNil(dot)
    }
}
