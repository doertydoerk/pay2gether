//
//  AccountHelperTest.swift
//  pay2getherTests
//
//  Created by Dirk Gerretz on 25.11.17.
//  Copyright © 2017 [code2 app];. All rights reserved.
//

@testable import pay2gether
import XCTest

class AccountHelperTest: BaseTest {
    // MARK: - Properties

    // MARK: - Tests

    func testCurrentAccountFromAllTransactions() {
        helper.createTestTransactions(8)
        let account = AccountHelper.getAccount(transactions: dataManager.fetchAllTransactions())

        XCTAssertTrue(account.allTransctions.count == 8)

        // Spender One
        for transaction in account.spender1Transactions {
            XCTAssertTrue(transaction.spender == "name1")
        }
        XCTAssertTrue(account.spender1Balance == 40)

        // Spender Two
        for transaction in account.spender2Transactions {
            XCTAssertTrue(transaction.spender == "name2")
        }
        XCTAssertTrue(account.spender2Balance == 40)

        XCTAssertTrue(account.spender1Percentage == 50)
        XCTAssertTrue(account.spender2Percentage == 50)
    }

    func testGetIndividualHistory() {
        helper.createTestTransactions(8)
        let transactions = dataManager.fetchAllTransactions()

        XCTAssertTrue(transactions.count == 8)
        let name1History = AccountHelper.getHistoryFor(spender: "name1",
                                                       inTransactions: transactions)

        XCTAssertTrue(name1History.count == 4)
        for transaction in name1History {
            XCTAssertTrue(transaction.spender == "name1")
        }

        let name2History = AccountHelper.getHistoryFor(spender: "name2",
                                                       inTransactions: transactions)

        XCTAssertTrue(name2History.count == 4)
        for transaction in name2History {
            XCTAssertTrue(transaction.spender == "name2")
        }
    }

    func testCalculateBalance() {
        helper.createTestTransactions(8)
        let transactions = dataManager.fetchAllTransactions()
        let balance = AccountHelper.balance(transactions)
        XCTAssertTrue(balance == 80)
    }

    func testPercentagePerSpender() {
        var result = AccountHelper.percentagePerSpender(20, 20)
        XCTAssertTrue(result == (50.0, 50.0))

        result = AccountHelper.percentagePerSpender(130, 70)
        XCTAssertTrue(result == (65.0, 35.0))
    }

    func testShouldDeleteHistory() {
        Config.threshold = 2
        XCTAssertFalse(AccountHelper.isWithinThreshold(13, 10), "Return value should be false")
        XCTAssertTrue(AccountHelper.isWithinThreshold(12, 10), "Return value should be true")
        XCTAssertTrue(AccountHelper.isWithinThreshold(10, 10), "Return value should be true")
        XCTAssertTrue(AccountHelper.isWithinThreshold(8, 10), "Return value should be true")
        XCTAssertFalse(AccountHelper.isWithinThreshold(7, 10), "Return value should be false")
    }

    func testIsWithinThreshold() {
        Config.threshold = 2
        XCTAssertTrue(AccountHelper.isWithinThreshold(2, 4), "Should return true")
        XCTAssertTrue(AccountHelper.isWithinThreshold(4, 2), "Should return true")
        XCTAssertFalse(AccountHelper.isWithinThreshold(5, 2), "Should return false")
    }

    func testIsWithinThresholdEarlyReturn() {
        Config.threshold = 0
        XCTAssertFalse(AccountHelper.isWithinThreshold(2, 2), "Should return false")
    }
}
