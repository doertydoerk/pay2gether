# Increment Build Number

PROJECT_NAME="pay2gether"
COMPLICATION="pay2getherComplication"
EXTENSION="pay2getherComplication Extension"
PLIST_MAIN="$PWD/$PROJECT_NAME/Info.plist"
PLIST_COMPLICATION="$PWD/$COMPLICATION/Info.plist"
PLIST_EXTENSION="$PWD/$EXTENSION/Info.plist"

PLB=/usr/libexec/PlistBuddy
NEW_VERSION=$(git rev-list --count HEAD)
echo ">>> Build: " $NEW_VERSION


$PLB -c "Set :CFBundleVersion $NEW_VERSION" "$PLIST_MAIN"
$PLB -c "Set :CFBundleVersion $NEW_VERSION" "$PLIST_EXTENSION"
$PLB -c "Set :CFBundleVersion $NEW_VERSION" "$PLIST_COMPLICATION"
