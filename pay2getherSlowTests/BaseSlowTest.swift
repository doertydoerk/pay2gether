//
//  BaseSlowTest.swift
//  pay2getherTests
//
//  Created by Dirk Gerretz on 13.11.17.
//  Copyright © 2017 [code2 app];. All rights reserved.
//

import CoreData
@testable import pay2gether
import XCTest

class BaseSlowTest: XCTestCase {
    // MARK: - Properties

    // MARK: - Prep

    override func setUp() {
        super.setUp()

        Config.testingMode = true

        // remove all prior items from context
        XCTAssertTrue(dataManager.deleteAllTransactions())
        // save empty context
        dataManager.persistData()

        // get Test Helper
        helper = TestHelper(dataManager: dataManager)
    }

    override func tearDown() {
        // remove all items from context
        _ = dataManager.deleteAllTransactions()
        // save empty context
        dataManager.persistData()

        Config.testingMode = false
        super.tearDown()
    }
}
