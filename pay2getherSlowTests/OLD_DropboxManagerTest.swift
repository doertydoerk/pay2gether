//
//  OLD_DropboxManagerTest.swift
//  pay2getherTests
//
//  Created by Dirk Gerretz on 12.11.17.
//  Copyright © 2017 [code2 app];. All rights reserved.
//

@testable import pay2gether
import XCTest

class OLD_DropboxManagerTest: BaseSlowTest {
    // MARK: - Properties

    // MARK: - Prep

    override func setUp() {
        super.setUp()
        XCTAssertTrue(Config.testingMode == true)
        XCTAssertTrue(dropboxManager.rootDir == "/test")
    }

    override func tearDown() {
        // clean up
        super.tearDown()
    }

    // MARK: - Tests

    func testReadAccessTokenFromFile() {
        let expected = "hDvXzUVO9DwAAAAAAAJbmm2yhYMfqnASuqGDt_Dk9123dofwadljfWzhlXqao9Td"
        let token = DropboxManager.readTokenFromPlist()
        XCTAssertTrue(token == expected)
    }

    // TODO: refacator as fails regulary
    func testWriteFileToDropbox() {
        let expt = expectation(description: "write to Dropbox")

        let ta = helper.createTestTransaction()
        let json = dropboxManager.jsonFrom(transaction: ta)
        let fileName = dropboxManager.filenameFrom(transaction: ta)
        dropboxManager.write(json: json,
                             withFilename: fileName,
                             atPath: dropboxManager.rootDir,
                             completionHandler: { _, _ in

                                 expt.fulfill()
                             })

        waitForExpectations(timeout: 5, handler: { _ in

        })
    }

    // TODO: refactor as fails frequently
    func DISABLED_testDeleteSingleTransactionFromDropbox() {
        var exists = true
        let expt1 = expectation(description: "exists")
        let ta = helper.createTestTransaction()
        let path = dropboxManager.filenameFrom(transaction: ta)

        //        dropboxManager.writeToDropbox(transaction: ta, completionHandler: nil)
        //        sleep(2)

        //		dropboxManager.removeTransaction(path: path, at: <#String?#>)
        dropboxManager.fileExists(path, completionHandler: { _, error in
            if error == nil {
                exists = false
                expt1.fulfill()
            }
        })
        waitForExpectations(timeout: 10, handler: { _ in
            XCTAssertFalse(exists)
        })
    }

    func testFilenameFromTransaction() {
        let ta = helper.createTestTransaction()
        let filename = dropboxManager.filenameFrom(transaction: ta)
        XCTAssertTrue(filename == "1511088122000000.json")
    }

    func testJsonFromTransaction() {
        let expected = "{\n  \"spender\" : \"name1\",\n  \"date\" : \"2017-11-19 10:42:02 +0000\",\n  \"amount\" : 10,\n  \"created\" : \"2017-11-19 10:42:02 +0000\",\n  \"purpose\" : \"Write Test\"\n}"
        let ta = helper.createTestTransaction()
        let result = dropboxManager.jsonFrom(transaction: ta)
        XCTAssertTrue(expected == result)
    }

    func DISABLE_testRemoveAllTransactionsFromDropbox() {
        let getFilesExpectation = expectation(description: "getFilesExpectation")
        var files: [Files.DeleteArg] = []

        // get current list of transactions
        let getFilesResponse: ListFolderResponse = { response, error in
            if let entries = response?.entries {
                for transaction in entries {
                    if let path = transaction.pathLower {
                        print(path)
                        files.append(Files.DeleteArg(path: transaction.pathLower!))
                    }
                }
                getFilesExpectation.fulfill()
            } else {
                print(error!)
            }
        }

        // create test data and wait for it to exist
        helper.createTestTransactions(5)
        sleep(3)

        // get all files in dropbox
        dropboxManager.getAll(files: getFilesResponse)
        wait(for: [getFilesExpectation], timeout: 5)

        // remove all files from Dropbox
        let removeFilesExpectation = expectation(description: "removeFilesExpectation")

        let removeAllResponse: RemoveAllResponse = { _, _ in
            print("deleting...")
            removeFilesExpectation.fulfill()
        }

        // delete all current transactions
        dropboxManager.removeAllFilesAt(path: files, completionHandler: removeAllResponse)
        wait(for: [removeFilesExpectation], timeout: 5)

        // assert folder is empty
        let allGone = expectation(description: "all files removed")
        var fail = 0
        for file in files {
            dropboxManager.fileExists(file.path, completionHandler: { _, error in
                if error != nil {
                    fail = fail + 1
                }
            })
        }

        if fail == 0 {
            allGone.fulfill()
        }

        wait(for: [allGone], timeout: 10)
        XCTAssertTrue(true)
    }
}
