//
//  SettingsProtocol.swift
//  pay2gether
//
//  Created by Dirk Gerretz on 12.01.18.
//  Copyright © 2018 [code2 app];. All rights reserved.
//

import Foundation

protocol SettingsViewProtocol {
    // nothing here yet
}

protocol SettingsPresenterProtocol {
    func resetHistory() -> Bool
}
