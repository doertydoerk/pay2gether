//
//  SettingsPresenter.swift
//  pay2gether
//
//  Created by Dirk Gerretz on 12.01.18.
//  Copyright © 2018 [code2 app];. All rights reserved.
//

import Foundation

class SettingsPresenter {
    // MARK: - Properties

    weak var view: SettingsViewController?
    var dataManager = CoreDataManager.shared

    // MARK: - Lifecycle

    convenience init(view: SettingsViewController?) {
        self.init()
        self.view = view
    }
}

extension SettingsPresenter: SettingsPresenterProtocol {
    func resetHistory() -> Bool {
        dataManager.deleteAllTransactions()
    }
}
