//
//  SettingsViewController.swift
//  pay2gether
//
//  Created by Dirk Gerretz on 12.01.18.
//  Copyright © 2018 [code2 app];. All rights reserved.
//

import UIKit

class SettingsViewController: UITableViewController {
    // MARK: - Properties

    var presenter: SettingsPresenter?
    @IBOutlet var name1Textfield: UITextField!
    @IBOutlet var name2Textfield: UITextField!
    @IBOutlet var thresholdTextfield: UITextField!

    // MARK: - Liefecycle

    override func viewDidLoad() {
        super.viewDidLoad()
        setupKeyboard()
        presenter = SettingsPresenter(view: self)
        navigationItem.title = "Settings"
        navigationController?.navigationBar.barStyle = UIBarStyle.black
        tableView.delegate = self
        loadSettings()
    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        saveSettings()
    }

    private func setupKeyboard() {
        name1Textfield.keyboardAppearance = .dark
        name2Textfield.keyboardAppearance = .dark
        thresholdTextfield.keyboardAppearance = .dark
        name1Textfield.becomeFirstResponder()
    }

    // MARK: - Load & Save

    func loadSettings() {
        name1Textfield.text = Config.name1
        name2Textfield.text = Config.name2
        thresholdTextfield.text = "\(Config.threshold)"
    }

    func saveSettings() {
        Config.name1 = (name1Textfield.text)!
        Config.name2 = (name2Textfield.text)!
        if thresholdTextfield.text != "" {
            Config.threshold = Int(thresholdTextfield.text!)!
        }
        _ = Config.writeUserDefaults()
    }

    // MARK: - Actions

    @IBAction func resetHistoryTapped(_: UIButton) {
        if (presenter?.resetHistory())! {
            issueDeletionAlert()
        }
    }

    func issueDeletionAlert() {
        let title = "Reset History"
        let message = "Done!"
        let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
        present(alert, animated: true, completion: nil)
    }
}
