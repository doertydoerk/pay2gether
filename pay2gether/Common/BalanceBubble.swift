import ChameleonFramework
import UIKit

@IBDesignable class BalanceBubble: UIView {
    // MARK: - Properties

    var label1 = UILabel()
    var label2 = UILabel()
    var label3 = UILabel()
    var font = UIFont(name: "HelveticaNeue-Light", size: 25.0)

    let labels: [UILabel] = []

    // MARK: - Lifecycle

    override func layoutSubviews() {
        super.layoutSubviews()
        layer.cornerRadius = frame.size.width / 2
        setupStackView()
    }

    func setupStackView() {
        // label1.backgroundColor = .darkGray
        label1.textAlignment = NSTextAlignment.center
        label1.textColor = .white
        label1.font = font

        // label2.backgroundColor = .darkGray
        label2.textAlignment = NSTextAlignment.center
        label2.textColor = .white
        label2.font = font

        // label3.backgroundColor = .darkGray
        label3.textAlignment = NSTextAlignment.center
        label3.textColor = .white
        label3.font = font

        let stackView = UIStackView(frame: CGRect(x: frame.size.width * 0.1,
                                                  y: frame.size.height * 0.25,
                                                  width: frame.size.width * 0.8,
                                                  height: frame.size.height * 0.5))

        stackView.alignment = UIStackView.Alignment.center
        stackView.axis = .vertical
        stackView.distribution = .fillEqually
        stackView.alignment = .fill
        stackView.spacing = 2

        stackView.addArrangedSubview(label1)
        stackView.addArrangedSubview(label2)
        stackView.addArrangedSubview(label3)

        addSubview(stackView)
    }
}
