import Foundation

struct TestHelper {
    let dataManager: CoreDataManager!

    init(dataManager: CoreDataManager) {
        self.dataManager = dataManager
    }

    func createTestTransactions(_ numberOfItem: Int) {
        for i in 0 ..< numberOfItem {
            let transaction = dataManager.newTransaction()

            transaction.amount = 10
            transaction.date = NSDate()
            if i % 2 == 0 {
                transaction.spender = "name1"
            } else {
                transaction.spender = "name2"
            }

            transaction.purpose = "Test \(i)"
        }
        dataManager.persistData()
    }

    func createTestTransaction() -> Transaction {
        let date = standardTestDate() as NSDate
        let ta = dataManager.newTransaction()
        ta.amount = 10
        ta.date = date
        ta.spender = "name1"
        ta.purpose = "Write Test"
        ta.created = date
        return ta
    }

    func standardTestDate() -> Date {
        let dateString = "2017-11-19 10:42:02 +0000"
        let dateFormatter = "yyyy-MM-dd HH:mm:ss ZZZ"
        let date = dateString.toDate(dateFormat: dateFormatter)
        return date
    }
}
