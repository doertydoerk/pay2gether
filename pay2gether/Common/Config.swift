import Foundation

enum Config {
    // MARK: - Properties

    static let defaults = UserDefaults.standard
    static var name1 = "John"
    static var name2 = "Jane"
    static let allSpender = ""
    static var threshold = 0
    static var testingMode = false

    // MARK: -

    static func writeUserDefaults() -> Bool {
        defaults.set(name1, forKey: "name1")
        defaults.set(name2, forKey: "name2")
        defaults.set(threshold, forKey: "threshold")
        return defaults.synchronize()
    }

    static func readUserDefaults() {
        if let _spender1 = defaults.value(forKey: "name1") {
            name1 = _spender1 as! String
        }

        if let _spender2 = defaults.value(forKey: "name2") {
            name2 = _spender2 as! String
        }

        if let _threshold = defaults.value(forKey: "threshold") {
            threshold = _threshold as! Int
        }
    }
}
