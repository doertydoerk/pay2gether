import Foundation
import WatchConnectivity

class WatchSync: NSObject {
    // MARK: - Properties

    fileprivate var watchSession: WCSession?

    // MARK: - Lifecycle

    override init() {
        super.init()
        if WCSession.isSupported() {
            watchSession = WCSession.default
            watchSession?.delegate = self
            watchSession?.activate()
        } else {
            print(">>> Session not supported")
        }
    }

    // MARK: - update watch

    func updateWatchWith(_ info: [String: String]) {
        guard watchSession?.isWatchAppInstalled == true else {
            print(">>> Watch app not installed")
            return
        }
        watchSession?.transferCurrentComplicationUserInfo(info)
    }
}

// MARK: WCSessionDelegate

extension WatchSync: WCSessionDelegate {
    func session(_: WCSession, activationDidCompleteWith _: WCSessionActivationState, error _: Error?) {
        print(">>> Session activation did complete")
        DispatchQueue.main.async { () in
            (CoreDataManager.shared).updateWatch()
        }
    }

    public func sessionDidBecomeInactive(_: WCSession) {
        print(">>> Session did become inactive")
    }

    public func sessionDidDeactivate(_: WCSession) {
        print(">>> Session did deactivate")
    }
}
