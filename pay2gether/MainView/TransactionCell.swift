//
//  TransactionCell.swift
//  pay2gether
//
//  Created by Dirk Gerretz on 29.11.17.
//  Copyright © 2017 [code2 app];. All rights reserved.
//

import UIKit

class TransactionCell: UITableViewCell {
    // MARK: - Properties

    @IBOutlet var dateLabel: UILabel!
    @IBOutlet var purposeLabel: UILabel!
    @IBOutlet var amountLabel: UILabel!
    var created: NSDate!
}
