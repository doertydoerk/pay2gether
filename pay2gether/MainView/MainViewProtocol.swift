//
//  MainViewProtocol.swift
//  pay2gether
//
//  Created by Dirk Gerretz on 29.11.17.
//  Copyright © 2017 [code2 app];. All rights reserved.
//

import Foundation

protocol MainViewProtocol {
    func requestUpdate()
    func issueThresholdAlert()
}

protocol MainPresenterProtocol {
    func requestAccountUpdate() -> Account
    func transactionHistoryFor(spender: String?) -> [Transaction]
    func deleteTransactionWith(creationDate: NSDate)
    func shouldDeleteHistory(_ account: Account) -> Bool
    func deleteAllTransactions()
    func segueToEditItem(_ dto: TransactionDTO)
    func segueToNewItemFor(spender: UInt)
    func segueToSettingsView()
}
