//
//  MainViewController.swift
//  pay2gether
//
//  Created by Dirk Gerretz on 06.11.17.
//  Copyright © 2017 [code2 app];. All rights reserved.
//

import ChameleonFramework
import UIKit

class MainViewController: UIViewController {
    // MARK: - Properties

    var presenter: MainPresenter!
    @IBOutlet var dashboard: UIView!
    @IBOutlet var tableView: UITableView!
    @IBOutlet var settingsButton: UIButton!

    // Bubbles
    @IBOutlet var bubble1: BalanceBubble!
    @IBOutlet var bubble1Container: UIView!
    var bubble1BigRect = CGRect()
    var bubble1SmallRect = CGRect()

    @IBOutlet var bubble2: BalanceBubble!
    @IBOutlet var bubble2Container: UIView!
    var bubble2BigRect = CGRect()
    var bubble2SmallRect = CGRect()

    // Gesture Recognizer
    var tapRecognizer1: UITapGestureRecognizer!
    var tapRecognizer2: UITapGestureRecognizer!
    var longPressRecognizer1: UILongPressGestureRecognizer!
    var longPressRecognizer2: UILongPressGestureRecognizer!

    var historyDisplayed: [Transaction] = []
    var historyForSpender = Config.allSpender

    // MARK: - Lifecycle

    override func viewDidLoad() {
        super.viewDidLoad()
        presenter = MainPresenter(view: self)

        bubble1.backgroundColor = .spender1MainColor
        bubble2.backgroundColor = .spender2MainColor

        // store frame for original bubble for later use
        bubble1BigRect = bubble1Container.frame
        bubble2BigRect = bubble2Container.frame

        setupGestureRecognizer()
        calculateSmallBubbleFrames()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(true, animated: false)
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        refresh(presenter.requestAccountUpdate())
    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        navigationController?.setNavigationBarHidden(false, animated: false)
    }

    // MARK: - Actions

    @IBAction func settingsTapped(_: UIButton) {
        presenter?.segueToSettingsView()
    }

    @objc func bubbleTapped(_ sender: UITapGestureRecognizer) {
        if sender == tapRecognizer1 {
            presenter.segueToNewItemFor(spender: 1)
        }

        if sender == tapRecognizer2 {
            presenter.segueToNewItemFor(spender: 2)
        }
    }

    @objc func longPressed(_ sender: UILongPressGestureRecognizer) {
        if !gestureStateIsBegan(gesture: sender) { return }

        if sender == longPressRecognizer1 {
            if historyForSpender == Config.name1 {
                historyDisplayed = presenter.transactionHistoryFor(spender: nil)
                historyForSpender = Config.allSpender
            } else {
                historyDisplayed = presenter.transactionHistoryFor(spender: "name1")
                historyForSpender = Config.name1
            }
        }

        if sender == longPressRecognizer2 {
            if historyForSpender == Config.name2 {
                historyDisplayed = presenter.transactionHistoryFor(spender: nil)
                historyForSpender = Config.allSpender
            } else {
                historyDisplayed = presenter.transactionHistoryFor(spender: "name2")
                historyForSpender = Config.name2
            }
        }

        tableView.reloadData()
    }

    func gestureStateIsBegan(gesture: UILongPressGestureRecognizer) -> Bool {
        if gesture.state == .began {
            return true
        } else {
            return false
        }
    }

    // MARK: - Size Bubbles.

    func makeSmall(bubble: Int) {
        if bubble == 1 {
            bubble1Container.frame = bubble1SmallRect
        }

        if bubble == 2 {
            bubble2Container.frame = bubble2SmallRect
        }
    }

    func makeBig(bubble: Int) {
        if bubble == 1 {
            bubble1Container.frame = bubble1BigRect
        }

        if bubble == 2 {
            bubble2Container.frame = bubble2BigRect
        }
    }

    private func calculateSmallBubbleFrames() {
        let size = CGSize(width: bubble1Container.frame.size.width * 0.7,
                          height: bubble1Container.frame.size.height * 0.7)
        bubble1SmallRect.size = size
        bubble2SmallRect.size = size

        bubble1SmallRect.origin = CGPoint(x: bubble1Container.frame.origin.x + 20,
                                          y: bubble1Container.frame.origin.y + 20)

        bubble2SmallRect.origin = CGPoint(x: bubble2Container.frame.origin.x + 50,
                                          y: bubble2Container.frame.origin.y + 50)
    }

    func resizeBubbles(account: Account) {
        if account.spender1Balance > account.spender2Balance {
            makeBig(bubble: 1)
            makeSmall(bubble: 2)
        } else if account.spender2Balance > account.spender1Balance {
            makeSmall(bubble: 1)
            makeBig(bubble: 2)
        } else {
            makeSmall(bubble: 1)
            makeSmall(bubble: 2)
        }
    }

    func setupGestureRecognizer() {
        tapRecognizer1 = UITapGestureRecognizer(target: self, action: #selector(bubbleTapped(_:)))
        bubble1Container.addGestureRecognizer(tapRecognizer1)
        tapRecognizer2 = UITapGestureRecognizer(target: self, action: #selector(bubbleTapped(_:)))
        bubble2Container.addGestureRecognizer(tapRecognizer2)

        longPressRecognizer1 = UILongPressGestureRecognizer(target: self, action: #selector(longPressed(_:)))
        bubble1Container.addGestureRecognizer(longPressRecognizer1)
        longPressRecognizer2 = UILongPressGestureRecognizer(target: self, action: #selector(longPressed(_:)))
        bubble2Container.addGestureRecognizer(longPressRecognizer2)
    }

    func issueThresholdAlert() {
        let title = "Reset History?"
        let message = "Spenders are only € \(Config.threshold) apart. Do you want to reset all data?"
        let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: { _ in
            self.presenter.deleteAllTransactions()
        }))
        alert.addAction(UIAlertAction(title: "Cancel", style: UIAlertAction.Style.cancel, handler: nil))
        present(alert, animated: true, completion: nil)
    }

    // MARK: - TableView

    func tableView(_: UITableView, canEditRowAt _: IndexPath) -> Bool {
        true
    }

    func tableView(_ tableView: UITableView,
                   commit editingStyle: UITableViewCell.EditingStyle,
                   forRowAt indexPath: IndexPath)
    {
        if editingStyle == UITableViewCell.EditingStyle.delete {
            let cell = tableView.cellForRow(at: indexPath) as! TransactionCell
            presenter.deleteTransactionWith(creationDate: cell.created)
            refresh(presenter.requestAccountUpdate())
        }
    }

    func refresh(_ account: Account) {
        // Bubble 1
        bubble1.label1.text = Config.name1
        bubble1.label2.text = "\(account.spender1Percentage) %"
        bubble1.label3.text = account.diffFor(spender: "name1")
        if bubble1.label3.text == "" {
            bubble1.label3.isHidden = true
        } else {
            bubble1.label3.isHidden = false
        }

        // Bubble 2
        bubble2.label1.text = Config.name2
        bubble2.label2.text = "\(account.spender2Percentage) %"
        bubble2.label3.text = account.diffFor(spender: "name2")
        if bubble2.label3.text == "" {
            bubble2.label3.isHidden = true
        } else {
            bubble2.label3.isHidden = false
        }

        resizeBubbles(account: account)
        updateTableView()

        guard account.allTransctions.count > 0 else {
            return
        }

        if presenter.shouldDeleteHistory(account) {
            issueThresholdAlert()
        }
    }

    func updateTableView() {
        historyDisplayed = presenter.transactionHistoryFor(spender: nil)
        historyForSpender = Config.allSpender
        tableView.reloadData()
    }
}

extension MainViewController: UITableViewDataSource {
    func numberOfSections(in _: UITableView) -> Int {
        1
    }

    func tableView(_: UITableView, numberOfRowsInSection _: Int) -> Int {
        historyDisplayed.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! TransactionCell

        cell.created = historyDisplayed[indexPath.row].created!
        if let _date = historyDisplayed[indexPath.row].date {
            cell.dateLabel.text = NSDate.toString(_date)
        }
        cell.purposeLabel.text = historyDisplayed[indexPath.row].purpose
        cell.amountLabel?.text = "\(historyDisplayed[indexPath.row].amount) €"

        if historyDisplayed[indexPath.row].spender == "name1" {
            cell.amountLabel.textColor = .spender1MainColor
        } else {
            cell.amountLabel.textColor = .spender2MainColor
        }

        return cell
    }

    func tableView(_: UITableView, titleForHeaderInSection _: Int) -> String? {
        switch historyForSpender {
        case Config.name1:
            return "\(Config.name1)'s Transactions "
        case Config.name2:
            return "\(Config.name2)'s Transactions "
        default:
            return "All Transactions "
        }
    }
}

extension MainViewController: MainViewProtocol {
    func requestUpdate() {
        refresh(presenter.requestAccountUpdate())
    }
}
