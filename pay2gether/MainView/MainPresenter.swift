//
//  MainPresenter.swift
//  pay2gether
//
//  Created by Dirk Gerretz on 02.12.17.
//  Copyright © 2017 [code2 app];. All rights reserved.
//

import UIKit

class MainPresenter: NSObject {
    // MARK: - Properties

    var view: MainViewController?
    let dataManager = CoreDataManager.shared

    // MARK: - Liefecycle

    convenience init(view: MainViewController) {
        self.init()
        self.view = view
        self.view?.tableView.delegate = self
    }

    override init() {
        super.init()
    }

    func dtoForTransaction(withCreationDate date: NSDate) -> TransactionDTO? {
        // get actual transaction for creation date
        if let ta = dataManager.findTransactionBy(creationDate: date) {
            // assign values to DTO
            var dto = TransactionDTO()
            dto.created = ta.created
            dto.date = ta.date
            dto.spender = ta.spender
            dto.purpose = ta.purpose
            dto.amount = ta.amount
            return dto
        }

        return nil
    }
}

extension MainPresenter: MainPresenterProtocol {
    @objc func segueToNewItemFor(spender: UInt) {
        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
        let newItemViewController = storyBoard.instantiateViewController(withIdentifier: "DetailView") as! DetailViewController

        // prepare new view
        newItemViewController.dto = TransactionDTO()

        if spender == 1 {
            newItemViewController.dto?.spender = "name1"
        } else if spender == 2 {
            newItemViewController.dto?.spender = "name2"
        }

        view?.navigationController?.pushViewController(newItemViewController, animated: true)
    }

    @nonobjc func segueToEditItem(_ dto: TransactionDTO) {
        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
        let editItemViewController = storyBoard.instantiateViewController(withIdentifier: "DetailView") as! DetailViewController
        editItemViewController.dto = dto
        view?.navigationController?.pushViewController(editItemViewController, animated: true)
    }

    @objc func segueToSettingsView() {
        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
        let settingsViewController = storyBoard.instantiateViewController(withIdentifier: "SettingsViewID") as! SettingsViewController
        view?.navigationController?.setNavigationBarHidden(true, animated: false)
        view?.navigationController?.pushViewController(settingsViewController, animated: true)
    }

    func deleteTransactionWith(creationDate: NSDate) {
        if let ta = dataManager.findTransactionBy(creationDate: creationDate) {
            dataManager.delete(transaction: ta)
        }
    }

    @objc func deleteAllTransactions() {
        if dataManager.deleteAllTransactions() {
            view?.requestUpdate()
        }
    }

    func requestAccountUpdate() -> Account {
        let allTransactions = dataManager.fetchAllTransactions()
        let account = AccountHelper.getAccount(transactions: allTransactions)
        return account
    }

    func shouldDeleteHistory(_ account: Account) -> Bool {
        AccountHelper.isWithinThreshold(account.spender1Balance, account.spender2Balance)
    }

    func transactionHistoryFor(spender: String?) -> [Transaction] {
        let allTransactions: [Transaction] = dataManager.fetchAllTransactions()
        let allSortedTransaction = allTransactions.sorted(by: { Int(($0.date?.timeIntervalSince1970)!) >
                Int(($1.date?.timeIntervalSince1970)!)
        })
        if spender == nil { return allSortedTransaction }
        return allSortedTransaction.filter { $0.spender == spender }
    }
}

extension MainPresenter: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let created = (tableView.cellForRow(at: indexPath) as? TransactionCell)?.created {
            if let dto = dtoForTransaction(withCreationDate: created) {
                segueToEditItem(dto)
            }
        }
    }

    func tableView(_: UITableView, willDisplayHeaderView view: UIView, forSection _: Int) {
        let header = view as! UITableViewHeaderFooterView
        header.textLabel?.textColor = .lightGray
    }
}
