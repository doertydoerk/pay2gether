import Foundation

enum AccountHelper {
    // MARK: - Static functions

    static func getAccount(transactions: [Transaction]) -> Account {
        var account = Account()
        account.allTransctions = transactions

        // Spender One
        account.spender1Transactions = getHistoryFor(spender: "name1",
                                                     inTransactions: transactions)
        account.spender1Balance = balance(account.spender1Transactions)

        // Spender Two
        account.spender2Transactions = getHistoryFor(spender: "name2",
                                                     inTransactions: transactions)
        account.spender2Balance = balance(account.spender2Transactions)

        // Percentages
        var percentages = percentagePerSpender(account.spender1Balance, account.spender2Balance)
        account.spender1Percentage = percentages.0.roundOneDecimal()
        account.spender2Percentage = percentages.1.roundOneDecimal()

        return account
    }

    static func getHistoryFor(spender: String, inTransactions: [Transaction]) -> [Transaction] {
        var history: [Transaction] = []
        history = inTransactions.filter { $0.spender == spender }
        return history
    }

    static func balance(_ transactions: [Transaction]) -> Int {
        var balance = 0

        for transaction in transactions {
            balance = balance + Int(transaction.amount)
        }

        return balance
    }

    static func percentagePerSpender(_ balanceOne: Int, _ balanceTwo: Int) -> (Double, Double) {
        guard balanceOne > 0 || balanceTwo > 0 else {
            return (0, 0)
        }
        let one = Double(balanceOne)
        let two = Double(balanceTwo)
        let total = one + two
        let spenderOnePercentage = (one * 100) / total
        let spenderTwoPercentage = (two * 100) / total

        return (spenderOnePercentage, spenderTwoPercentage)
    }

    static func isWithinThreshold(_ number1: Int, _ number2: Int) -> Bool {
        let threshold = Config.threshold

        guard threshold > 0 else { return false }

        let diff = number1 - number2
        if diff <= threshold, diff >= 0 {
            return true
        } else if diff >= -threshold, diff <= 0 {
            return true
        }
        return false
    }
}
