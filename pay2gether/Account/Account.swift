import Foundation

struct Account {
    // MARK: - Properties

    var allTransctions: [Transaction] = []
    var spender1Transactions: [Transaction] = []
    var spender2Transactions: [Transaction] = []

    var spender1Balance = 0
    var spender2Balance = 0
    var spender1Percentage = 0.0
    var spender2Percentage = 0.0
    var spender1IsUpBy = ""
    var spender2IsUpBy = ""

    func diffFor(spender: String) -> String {
        if spender == "name1" {
            if spender1Balance > spender2Balance {
                return "+ \(spender1Balance - spender2Balance) €"
            }
        }

        if spender == "name2" {
            if spender2Balance > spender1Balance {
                return "+ \(spender2Balance - spender1Balance) €"
            }
        }
        return ""
    }
}
