//
//  CoreDataManager.swift
//  pay2gether
//
//  Created by Dirk Gerretz on 12.11.17.
//  Copyright © 2017 [code2 app];. All rights reserved.
//

import CoreData
import UIKit

class CoreDataManager {
    // MARK: - Properties

    static let shared = CoreDataManager()
    let entityType = "Transaction"
    let context = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
    var fetchAllItems: NSFetchRequest<NSFetchRequestResult>!
    let watchUpdater = WatchSync()

    // MARK: - Lifecycle

    init() {
        fetchAllItems = NSFetchRequest<NSFetchRequestResult>(entityName: entityType)
    }

    // MARK: - Misc

    func findTransactionBy(creationDate: NSDate) -> Transaction? {
        let transactions = fetchAllTransactions()

        for transaction in transactions {
            let transactionDate = transaction.created?.timeIntervalSince1970
            let searchDate = creationDate.timeIntervalSince1970

            if transactionDate == searchDate { return transaction }
        }
        return nil
    }

    // MARK: - Core Data

    func persistData() {
        do {
            try context.save()
            updateWatch()
        } catch {
            print(">>> Failed to save context: \(error)")
        }
    }

    func updateWatch() {
        let allTransactions = fetchAllTransactions()
        let account = AccountHelper.getAccount(transactions: allTransactions)
        var update = ["spender": "", "amount": ""]

        if account.spender1Balance > account.spender2Balance {
            update["spender"] = Config.name1
            update["amount"] = account.diffFor(spender: "name1")
        } else if account.spender2Balance > account.spender1Balance {
            update["spender"] = Config.name2
            update["amount"] = account.diffFor(spender: "name2")
        } else {
            update["spender"] = ""
            update["amount"] = ""
        }

        watchUpdater.updateWatchWith(update)
    }

    func delete(transaction: Transaction) {
        context.delete(transaction)
        persistData()
    }

    func deleteAllTransactions() -> Bool {
        let deleteAll = NSBatchDeleteRequest(fetchRequest: fetchAllItems)
        do {
            try context.execute(deleteAll)
            persistData()
        } catch {
            print(">>> Delete all items failed: \(error)")
        }

        if countTransactions() == 0 {
            print(">>> deleted")
            return true
        } else {
            return false
        }
    }

    func countTransactions() -> Int {
        var count = 0

        do {
            count = try context.count(for: fetchAllItems)
        } catch {
            print(">>> Fetch request failed: \(error)")
        }
        return count
    }
}

extension CoreDataManager: CoreDataManagerProtocol {
    func newTransaction() -> Transaction {
        let ta = NSEntityDescription.insertNewObject(forEntityName: entityType, into: context) as! Transaction
        ta.created = NSDate()
        return ta
    }

    func fetchAllTransactions() -> [Transaction] {
        var transactions: [Transaction] = []

        do {
            transactions = try context.fetch(fetchAllItems) as! [Transaction]
        } catch {
            print(">>> Fetch request failed: \(error)")
        }

        return transactions
    }
}
