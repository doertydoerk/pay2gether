//
//  TransactionDTO.swift
//  pay2gether
//
//  Created by Dirk Gerretz on 17.12.17.
//  Copyright © 2017 [code2 app];. All rights reserved.
//

import Foundation

struct TransactionDTO {
    var created: NSDate?
    var spender: String? = ""
    var amount: Int32 = 0
    var date: NSDate? = NSDate()
    var purpose: String? = ""
}
