//
//  Transaction+CoreDataProperties.swift
//  pay2gether
//
//  Created by Dirk Gerretz on 17.12.17.
//  Copyright © 2017 [code2 app];. All rights reserved.
//
//

import CoreData
import Foundation

public extension Transaction {
    @nonobjc class func fetchRequest() -> NSFetchRequest<Transaction> {
        NSFetchRequest<Transaction>(entityName: "Transaction")
    }

    @NSManaged var amount: Int32
    @NSManaged var date: NSDate?
    @NSManaged var purpose: String?
    @NSManaged var spender: String?
    @NSManaged var created: NSDate?
}
