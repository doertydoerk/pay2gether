//
//  CoreDataManagerProtocol.swift
//  pay2gether
//
//  Created by Dirk Gerretz on 03.12.17.
//  Copyright © 2017 [code2 app];. All rights reserved.
//

import Foundation

protocol CoreDataManagerProtocol {
    func fetchAllTransactions() -> [Transaction]
    func newTransaction() -> Transaction
}
