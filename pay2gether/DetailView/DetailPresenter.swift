//
//  DetailPresenter.swift
//  pay2gether
//
//  Created by Dirk Gerretz on 02.12.17.
//  Copyright © 2017 [code2 app];. All rights reserved.
//

import Foundation

class DetailPresenter {
    // MARK: - Properties

    let dataManager = CoreDataManager.shared
    var view: DetailViewController?

    // MARK: - Lifecycle

    init(view: DetailViewController?) {
        self.view = view
    }

    // MARK: -

    func newTransactionFrom(_ dto: TransactionDTO) {
        let ta = dataManager.newTransaction()
        ta.created = NSDate()
        ta.amount = dto.amount
        ta.date = dto.date
        ta.purpose = dto.purpose
        ta.spender = dto.spender

        dataManager.persistData()
    }

    func update(transaction _: Transaction, with dto: TransactionDTO) {
        let ta = dataManager.findTransactionBy(creationDate: dto.created!)
        if let _ta = ta {
            _ta.date = dto.date
            if dto.spender == "name1" {
                _ta.spender = "name1"
            } else if dto.spender == "name2" {
                _ta.spender = "name2"
            }

            _ta.purpose = dto.purpose
            _ta.amount = dto.amount
            dataManager.persistData()
        }
    }
}

extension DetailPresenter: DetailPresenterProtocol {
    func save(_ dto: TransactionDTO) {
        if dto.created != nil {
            if let _ta = dataManager.findTransactionBy(creationDate: dto.created!) {
                update(transaction: _ta, with: dto)
            }
        } else {
            newTransactionFrom(dto)
        }

        view?.navigationController?.popViewController(animated: true)
    }
}
