//
//  DetailProtocol.swift
//  pay2gether
//
//  Created by Dirk Gerretz on 02.12.17.
//  Copyright © 2017 [code2 app];. All rights reserved.
//

import Foundation

protocol DetailViewProtocol {
    func showIncompleteItemAlert()
}

protocol DetailPresenterProtocol {
    func save(_ dto: TransactionDTO)
}
