//
//  DetailViewController.swift
//  pay2gether
//
//  Created by Dirk Gerretz on 29.11.17.
//  Copyright © 2017 [code2 app];. All rights reserved.
//

import RaisePlaceholder
import UIKit

class DetailViewController: UIViewController {
    // MARK: - Properties

    var presenter: DetailPresenter!
    var spenderName: String!

    @IBOutlet var backButton: UIButton!
    @IBOutlet var doneButton: UIButton!
    @IBOutlet var bubble: Dot!
    @IBOutlet var textFieldContainer: UIView!
    @IBOutlet var dateTextfield: RaisePlaceholder!
    @IBOutlet var spenderTextfield: RaisePlaceholder!
    @IBOutlet var purposeTextfield: RaisePlaceholder!
    @IBOutlet var amountTextfield: RaisePlaceholder!
    @IBOutlet var datePicker: UIDatePicker!
    var dto: TransactionDTO?

    override func viewDidLoad() {
        super.viewDidLoad()
        setup(backButton)
        setup(doneButton)
        setDatePicker()

        presenter = DetailPresenter(view: self)
        populateTextfields()
        // adjust the cursor color
        UITextField.appearance().tintColor = .white
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(true, animated: false)
        setBubbleColors()
    }

    override func viewDidDisappear(_: Bool) {
        navigationController?.setNavigationBarHidden(false, animated: false)
        dto = nil
    }

    private func setup(_ button: UIButton) {
        button.layer.cornerRadius = 0.5 * button.bounds.size.width
        button.clipsToBounds = true
        button.setNeedsDisplay()
    }

    private func setDatePicker() {
        datePicker.addTarget(self, action: #selector(datePickerChanged(sender:)), for: UIControl.Event.valueChanged)
        datePicker.isHidden = true
        datePicker.setValue(UIColor.white, forKeyPath: "textColor")
    }

    fileprivate func setBubbleColors() {
        if spenderTextfield.text == Config.name1 {
            bubble.mainColor = .spender1MainColor
            doneButton.backgroundColor = .spender2MainColor
            backButton.backgroundColor = .spender2MainColor
            dateTextfield.underLineColor = .spender2MainColor
            spenderTextfield.underLineColor = .spender2MainColor
            purposeTextfield.underLineColor = .spender2MainColor
            amountTextfield.underLineColor = .spender2MainColor
        } else {
            bubble.mainColor = .spender2MainColor
            doneButton.backgroundColor = .spender1MainColor
            backButton.backgroundColor = .spender1MainColor
            dateTextfield.underLineColor = .spender1MainColor
            spenderTextfield.underLineColor = .spender1MainColor
            purposeTextfield.underLineColor = .spender1MainColor
            amountTextfield.underLineColor = .spender1MainColor
        }
        bubble.setNeedsDisplay()
        dateTextfield.setNeedsDisplay()
        spenderTextfield.setNeedsDisplay()
        purposeTextfield.setNeedsDisplay()
        amountTextfield.setNeedsDisplay()
    }

    private func populateTextfields() {
        dateTextfield.text = NSDate.toString((dto?.date)!)
        dateTextfield.autocorrectionType = .no

        if dto?.spender == "name1" {
            spenderTextfield.text = Config.name1
        } else if dto?.spender == "name2" {
            spenderTextfield.text = Config.name2
        }

        purposeTextfield.text = dto?.purpose
        if (dto?.amount)! > 0 {
            amountTextfield.text = "\(String(describing: (dto?.amount)!))"
        } else {
            amountTextfield.text = ""
        }
        purposeTextfield.autocorrectionType = .no
        purposeTextfield.becomeFirstResponder()
    }

    @IBAction func doneTapped(_: UIButton) {
        saveItem()
    }

    @IBAction func backTapped(_: UIButton) {
        navigationController?.popViewController(animated: true)
    }

    func saveItem() {
        if !isValidTransaction() {
            return
        }

        view.currentFirstResponder()?.resignFirstResponder()
        presenter.save(dto!)
    }

    func isValidTransaction() -> Bool {
        guard dateTextfield.text != "" else {
            issueMissingDetailAlert("DATE")
            return false
        }

        guard spenderTextfield.text != "" else {
            issueMissingDetailAlert("SPENDER")
            return false
        }

        guard purposeTextfield.text != "" else {
            issueMissingDetailAlert("PURPOSE")
            return false
        }

        guard amountTextfield.text != "" else {
            issueMissingDetailAlert("n AMOUNT")
            return false
        }

        guard amountTextfield.text != "0" else {
            issueMissingDetailAlert("n AMOUNT")
            return false
        }

        return true
    }

    private func issueMissingDetailAlert(_ detail: String) {
        let title = "Incomplete Transaction"
        let message = "Please enter a " + detail + " or go back to the main screen."

        let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
        present(alert, animated: true, completion: nil)
    }

    @objc func datePickerChanged(sender _: UIDatePicker) {
        dateTextfield.text = Date.toString(datePicker.date)
        dto?.date = datePicker.date as NSDate
    }
}

extension DetailViewController: UITextFieldDelegate {
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        if textField != dateTextfield {
            datePicker.isHidden = true
        }

        guard textField == spenderTextfield || textField == dateTextfield else {
            return true
        }

        if textField == spenderTextfield {
            if textField.text == Config.name1 {
                dto?.spender = "name2"
                textField.text = Config.name2
            } else {
                dto?.spender = "name1"
                textField.text = Config.name1
            }

            setBubbleColors()
        }

        if textField == dateTextfield {
            textField.resignFirstResponder()
            datePicker.fadeIn()
            view.currentFirstResponder()?.resignFirstResponder()
            datePicker.isHidden = false
        }

        return false
    }

    func textFieldDidEndEditing(_ textField: UITextField) {
        switch textField {
        case purposeTextfield:
            dto?.purpose = purposeTextfield.text
        default:
            if amountTextfield.text != "" {
                dto?.amount = Int32((amountTextfield.text)!)!
            }
            // FIX ME:  dead code?
            //            if (dto?.amount)! > 0 {
            //                amountTextfield.text = "\(String(describing: (dto?.amount)!))"
            //            }
        }
    }

    func textFieldShouldReturn(_: UITextField) -> Bool {
        saveItem()
        return false
    }
}
