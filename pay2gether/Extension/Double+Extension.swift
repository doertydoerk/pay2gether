//
//  Double+Extension.swift
//  pay2gether
//
//  Created by Dirk Gerretz on 16.12.17.
//  Copyright © 2017 [code2 app];. All rights reserved.
//

import Foundation

extension Double {
    mutating func roundOneDecimal() -> Double {
        // 3 decimal = 10000, 2 decimal = 100, 1 deciaml = 10
        Double(Darwin.round(10 * self) / 10)
    }
}
