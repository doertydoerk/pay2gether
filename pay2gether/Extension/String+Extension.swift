//
//  String+Extension.swift
//  pay2gether
//
//  Created by Dirk Gerretz on 22.11.17.
//  Copyright © 2017 [code2 app];. All rights reserved.
//

import Foundation

extension String {
    func toDate(dateFormat format: String) -> Date {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = format
        if let date = dateFormatter.date(from: self) {
            return date
        }
        print("Invalid arguments ! Returning Current Date.")
        return Date()
    }
}
