//
//  UIColor+Extension.swift
//  OM3Sample
//
//  Created by Dirk Gerretz on 22.08.17.
//  Copyright © 2017 AOE. All rights reserved.
//

import UIKit

extension UIColor {
    static var bodyText: UIColor {
        UIColor(hex: "29343B")
    }

    static var screenBackground: UIColor {
        UIColor(hex: "EFEFF4")
    }

    static var bubbleText: UIColor {
        UIColor(hex: "606A71")
    }

    static var systemMessage: UIColor {
        UIColor(hex: "8CC55B")
    }

    static var spender1MainColor: UIColor {
        UIColor(hex: "8CC55B")
    }

    static var spender2MainColor: UIColor {
        UIColor(hex: "F6B939")
    }
}

extension UIColor {
    convenience init(hex: String) {
        let scanner = Scanner(string: hex)
        var rgbValue: UInt64 = 0
        scanner.scanHexInt64(&rgbValue)
        let r = (rgbValue & 0xFF0000) >> 16
        let g = (rgbValue & 0xFF00) >> 8
        let b = rgbValue & 0xFF
        self.init(
            red: CGFloat(r) / 0xFF,
            green: CGFloat(g) / 0xFF,
            blue: CGFloat(b) / 0xFF, alpha: 1
        )
    }
}
