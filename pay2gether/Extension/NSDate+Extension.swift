//
//  NSDate+Extension.swift
//  pay2gether
//
//  Created by Dirk Gerretz on 22.11.17.
//  Copyright © 2017 [code2 app];. All rights reserved.
//

import Foundation

extension NSDate {
    static func microsecondsFrom(date: NSDate) -> UInt64 {
        let nowDouble = date.timeIntervalSince1970
        return UInt64(nowDouble * 1_000_000)
    }

    static func dateFromMicroseconds(sec: UInt64) -> NSDate {
        NSDate(timeIntervalSince1970: TimeInterval(sec / 1000))
    }

    static func toString(_ date: NSDate) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd-MMM-yyyy"
        return "\(dateFormatter.string(from: date as Date))"
    }
}

extension Date {
    static func toString(_ date: Date) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd-MMM-yyyy"
        return "\(dateFormatter.string(from: date))"
    }
}
