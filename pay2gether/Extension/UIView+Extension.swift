
//
//  UIView+Extension.swift
//  pay2gether
//
//  Created by Dirk Gerretz on 17.12.17.
//  Copyright © 2017 [code2 app];. All rights reserved.
//

import UIKit

extension UIView {
    func currentFirstResponder() -> UIView? {
        for view in subviews {
            if view.isFirstResponder {
                return view
            }

            for subview in view.subviews {
                if subview.isFirstResponder {
                    return subview
                }
            }
        }
        return nil
    }

    func fadeIn() {
        alpha = 0
        isHidden = false
        UIView.animate(withDuration: 0.3) {
            self.alpha = 1
        }
    }

    func fadeOut() {
        alpha = 1
        isHidden = false
        UIView.animate(withDuration: 0.3) {
            self.alpha = 0
        }
    }
}
