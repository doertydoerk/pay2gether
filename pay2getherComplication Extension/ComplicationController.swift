//
//  ComplicationController.swift
//  pay2getherComplication Extension
//
//  Created by Dirk Gerretz on 18.12.17.
//  Copyright © 2017 [code2 app];. All rights reserved.
//

import ClockKit

class ComplicationController: NSObject, CLKComplicationDataSource {
    // MARK: - Preoperties

    let textTemplate = { () -> CLKComplicationTemplateModularSmallStackText in
        let updater = WatchUpdater.shared
        let template = CLKComplicationTemplateModularSmallStackText()
        template.line1TextProvider = CLKSimpleTextProvider(text: (updater.latestUpdate["spender"])!)
        template.line2TextProvider = CLKSimpleTextProvider(text: (updater.latestUpdate["amount"])!)
        return template
    }

    let imageTemplate = { () -> CLKComplicationTemplateModularSmallSimpleImage in
        let template = CLKComplicationTemplateModularSmallSimpleImage()
        template.imageProvider = CLKImageProvider(onePieceImage: UIImage(named: "Bubbles")!)
        return template
    }

    // MARK: - Placeholder Templates

    func getLocalizableSampleTemplate(for complication: CLKComplication, withHandler handler: @escaping (CLKComplicationTemplate?) -> Void) {
        switch complication.family {
        case .modularSmall:
            handler(getTemplate())

        default:
            NSLog("%@", "Unknown complication type: \(complication.family)")
            handler(nil)
        }
    }

    // MARK: - Timeline Population

    func getCurrentTimelineEntry(for complication: CLKComplication, withHandler handler: @escaping (CLKComplicationTimelineEntry?) -> Void) {
        switch complication.family {
        case .modularSmall:
            handler(CLKComplicationTimelineEntry(date: Date(), complicationTemplate: getTemplate()))

        default:
            NSLog("%@", "Unknown complication type: \(complication.family)")
            handler(nil)
        }
    }

    func getSupportedTimeTravelDirections(for _: CLKComplication, withHandler handler: @escaping (CLKComplicationTimeTravelDirections) -> Void) {
        // disable time travel support by providing bo handler
        handler([])
    }

    // MARK: - Misc.

    func getTemplate() -> CLKComplicationTemplate {
        let updater = WatchUpdater.shared
        if updater.latestUpdate["spender"] == "" {
            return imageTemplate()
        }

        return textTemplate()
    }
}
