//
//  WatchUpdater.swift
//  pay2getherComplication Extension
//
//  Created by Dirk Gerretz on 21.12.17.
//  Copyright © 2017 [code2 app];. All rights reserved.
//

import ClockKit
import Foundation
import WatchConnectivity
import WatchKit

class WatchUpdater: NSObject {
    // MARK: - Properties

    let spender = "spender"
    let amount = "amount"
    let defaults = UserDefaults(suiteName: "group.com.code2app.pay2gether")
    static let shared = WatchUpdater()
    private var session: WCSession?
    var latestUpdate = ["spender": "", "amount": ""]

    // MARK: - Lifecycle

    override init() {
        super.init()
        setupWatchSession()
        latestUpdate = readBalance()
    }

    private func setupWatchSession() {
        let session = WCSession.default
        session.delegate = self
        session.activate()
    }

    private func write(balance: [String: String]) {
        defaults?.set(balance[spender], forKey: spender)
        defaults?.set(balance[amount], forKey: amount)
    }

    private func readBalance() -> [String: String] {
        var balance = [spender: "", amount: ""]

        guard (defaults?.string(forKey: spender)) != nil, (defaults?.string(forKey: amount)) != nil else {
            print(">>> no user defaults available")
            return balance
        }

        balance[spender] = defaults?.string(forKey: spender)
        balance[amount] = defaults?.string(forKey: amount)
        return balance
    }
}

extension WatchUpdater: WCSessionDelegate {
    func session(_: WCSession, activationDidCompleteWith _: WCSessionActivationState, error: Error?) {
        if error == nil {
            print(">>> Session activation did complete (\(String(describing: session?.activationState)))")
        } else {
            print(">>> \(error!)")
        }
    }

    func session(_: WCSession, didReceiveUserInfo userInfo: [String: Any] = [:]) {
        latestUpdate[spender] = userInfo[spender] as? String
        latestUpdate[amount] = String((userInfo[amount] as! String).dropFirst(2))
        write(balance: latestUpdate)

        let server = CLKComplicationServer.sharedInstance()
        for complication in server.activeComplications! {
            server.reloadTimeline(for: complication)
        }
    }
}
