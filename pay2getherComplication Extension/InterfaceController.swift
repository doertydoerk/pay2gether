//
//  InterfaceController.swift
//  pay2getherComplication Extension
//
//  Created by Dirk Gerretz on 18.12.17.
//  Copyright © 2017 [code2 app];. All rights reserved.
//

import Foundation
import WatchKit

class InterfaceController: WKInterfaceController {
    // MARK: - Properties

    @IBOutlet var spenderLabel: WKInterfaceLabel!
    @IBOutlet var amountLabel: WKInterfaceLabel!
    @IBOutlet var middleLabel: WKInterfaceLabel!
    let spender1Color = UIColor(red: 0.54, green: 0.78, blue: 0.33, alpha: 1.00)
    let spender2Color = UIColor(red: 0.97, green: 0.73, blue: 0.14, alpha: 1.00)
    let updater = WatchUpdater.shared

    // MARK: - Lifecycle

    override func awake(withContext context: Any?) {
        super.awake(withContext: context)
        // Configure interface objects here.
    }

    override func willActivate() {
        super.willActivate()
        setLabels()
    }

    override func didDeactivate() {
        // This method is called when watch view controller is no longer visible
        super.didDeactivate()
    }

    func setLabels() {
        if updater.latestUpdate["spender"] != "" {
            spenderLabel.setText(updater.latestUpdate["spender"])
            middleLabel.setText("is up by")
            amountLabel.setText(updater.latestUpdate["amount"])
        } else {
            spenderLabel.setText("fair")
            middleLabel.setText("'n")
            amountLabel.setText("square")
        }
        setLabelColor()
    }

    func setLabelColor() {
        switch updater.latestUpdate["spender"] {
        case Config.name1?:
            spenderLabel.setTextColor(spender1Color)
        case Config.name2?:
            spenderLabel.setTextColor(spender2Color)
        default:
            spenderLabel.setTextColor(.white)
        }
    }
}
